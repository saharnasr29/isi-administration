package com.example.isi;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class demande_fragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        String token = bundle.getString("tkn");

        View rootView = inflater.inflate(R.layout.fragment_demande, container, false);

        mRecyclerView = rootView.findViewById(R.id.recyclerView);

        GetProcessTask myTask = new GetProcessTask(token);

        myTask.execute();

        return rootView;

    }



    public class GetProcessTask extends AsyncTask<Void, Void, ArrayList<Exampledemande>> {
        private final String token;

        GetProcessTask(String tkn) {
            token = tkn;

        }

        @Override
        protected ArrayList<Exampledemande> doInBackground(Void... params) {
            ArrayList<Exampledemande> List = new ArrayList<>();
            OkHttpClient client = new OkHttpClient();


            String tkn = "Bearer " + token;
            Log.i("valeur token", tkn);
            Request request = new Request.Builder()
                    .url("http://process.isiforge.tn/api/1.0/isi/cases/participated")
                    .addHeader("Authorization", tkn)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response responses = null;
            Log.i("la requete", String.valueOf(request));
            try {
                responses = client.newCall(request).execute();

                String jsonData = responses.body().string();

                JSONArray jsonarray = new JSONArray(jsonData);
                //  Log.i("liste process", String.valueOf(jsonarray));


                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    // Log.i("objet process", String.valueOf(jsonobject));
                    String id = jsonobject.getString("app_uid");
                    String number = jsonobject.getString("app_title");
                    String process = jsonobject.getString("app_pro_title");
                    String status = jsonobject.getString("app_status_label");
                    String date = jsonobject.getString("app_update_date");

                    List.add(new Exampledemande(id, process, number, status, date));
                }
                Log.i("résultat", List.toString());

                return List;

            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return List;
            }

        }

        protected void onPostExecute(ArrayList<Exampledemande> result) {

            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getContext());
            mAdapter = new ExampleDemandeAdapter(result);

            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}
