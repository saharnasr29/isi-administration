package com.example.isi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

public class MenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Intent intent = getIntent();
        token = intent.getStringExtra("token");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putString("tkn", token);
            fairedemande_fragment add = new fairedemande_fragment();
            add.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    add).commit();
            navigationView.setCheckedItem(R.id.nav_add);
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Bundle arguments = new Bundle();
        arguments.putString("tkn", token);

        switch (item.getItemId()) {
            case R.id.nav_add:
                fairedemande_fragment add = new fairedemande_fragment();
                add.setArguments(arguments);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        add).commit();
                break;

            case R.id.nav_demande:
                demande_fragment demande = new demande_fragment();
                demande.setArguments(arguments);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        demande).commit();
                break;

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
