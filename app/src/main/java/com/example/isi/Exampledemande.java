package com.example.isi;

public class Exampledemande {

    private String process;
    private String id;
    private String number;
    private String status;
    private String last_update;

    public Exampledemande( String text1, String text2,String text3, String text4,String text5) {

        id = text1;
        process = text2;
        number = text3;
        status = text4;
        last_update = text5;
    }


    public String getProcess() {
        return process;
    }

    public String getNumber() {
        return number;
    }

    public String getStatus() {
        return status;
    }

    public String getLast_update() {
        return last_update;
    }

    public String getId() {
        return id;
    }

    public String toString(){
        return "Id : " + id + "\nTexte : " + process + "\ndate" + last_update;
    }
}
