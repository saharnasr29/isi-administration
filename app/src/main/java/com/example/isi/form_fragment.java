package com.example.isi;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class form_fragment extends Fragment {

    ArrayList<Exampleitem> itemList = new ArrayList<>();
    LinearLayout linearLayout;
    private APIService mAPIService;
    Context thiscontext;
    int total=1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        String token = bundle.getString("tkn");
        String id = bundle.getString("id");
        String titre = bundle.getString("titre");
        String tas = bundle.getString("tas");
        View rootView = inflater.inflate(R.layout.fragment_form, container, false);
        linearLayout = rootView.findViewById(R.id.linear_layout);
        thiscontext = container.getContext();
        GetDynaForm myTask = new GetDynaForm(token, id, tas);
        myTask.execute();



        return rootView;
    }




    public class GetDynaForm extends AsyncTask<Void, Void, ArrayList<Exampleitem>>  {
        private final String token;
        private final String pro_id;
        private final String tas_id;

        GetDynaForm(String tkn, String id, String tas) {
            token = tkn;
            pro_id = id;
            tas_id = tas;

        }

        @Override
        protected ArrayList<Exampleitem> doInBackground(Void... params) {


            //1er API 

            OkHttpClient client = new OkHttpClient();
            String tkn = "Bearer " + token;
            String url = "http://process.isiforge.tn/api/1.0/isi/project/" + pro_id + "/starting-tasks";
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Authorization", tkn)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response responses = null;
            Log.i("la requete", String.valueOf(request));
            try {
                responses = client.newCall(request).execute();

                String jsonData = responses.body().string();
                JSONArray jsonarray = new JSONArray(jsonData);
                JSONObject obj = jsonarray.getJSONObject(0);
                    Log.i("premier api", jsonData);
                String act_uid = obj.getString("act_uid");
                String act_name = obj.getString("act_name");

                //2émé API
                OkHttpClient client2 = new OkHttpClient();
                String url2 = "http://process.isiforge.tn/api/1.0/isi/project/" + pro_id + "/activity/" + act_uid + "/steps";
                Request request2 = new Request.Builder()
                        .url(url2)
                        .addHeader("Authorization", tkn)
                        .addHeader("Content-Type", "application/json")
                        .build();
                Response responses2 = null;
                try {
                    responses2 = client2.newCall(request2).execute();
Log.i("inside deuxieme", String.valueOf(responses2));
                    String jsonData2 = responses2.body().string();
                    Log.i("deuxieme api", jsonData2);
                    JSONArray jsonarray2 = new JSONArray(jsonData2);
                    JSONObject obj2 = jsonarray2.getJSONObject(0);
                    String step_uid_obj = obj2.getString("step_uid_obj");

                   //3émé API
                    OkHttpClient client3 = new OkHttpClient();
                    String url3 = "http://process.isiforge.tn/api/1.0/isi/extrarest/dynaform/" + step_uid_obj;
                    Request request3 = new Request.Builder()
                            .url(url3)
                            .addHeader("Authorization", tkn)
                            .addHeader("Content-Type", "application/json")
                            .build();
                    Response responses3 = null;
                    try {
                        responses3 = client3.newCall(request3).execute();

                        String jsonData3 = responses3.body().string();


                        JSONArray jsonarray3 = new JSONArray(jsonData3);
                        String type;
                        String id;
                        String label;
                        String placeholder;
                        JSONObject jo = new JSONObject();
                        jo.put("", "");
                        for (int i = 0; i < jsonarray3.length(); i++) {
                            JSONObject jsonobject = jsonarray3.getJSONObject(i);
                            if (jsonobject.has("type") && jsonobject.has("id") && jsonobject.has("label")) {
                                type = jsonobject.getString("type");
                                id = jsonobject.getString("id");
                                label = jsonobject.getString("label");
                                JSONArray options = new JSONArray();
                                if (jsonobject.has("placeholder")) {
                                placeholder = jsonobject.getString("placeholder"); }
                                else{placeholder ="";}
                                if (type.equals("image")) {
                                    label = jsonobject.getString("src");
                                }
                                if (jsonobject.has("options")) {
                                 options = jsonobject.getJSONArray("options");
                                 Log.i("options", String.valueOf(options));
                                     if(type.equals("dropdown") && options.length()==0) {
                                         String url4 = "http://process.isiforge.tn/api/1.0/isi/project/" + pro_id +"/process-variable/" + id + "/execute-query";
                                         RequestBody body =  new FormBody.Builder()
                                                 .add("dyn_uid",step_uid_obj)
                                                 .add("field_id", id)
                                                 .build();
                                         Request request4 = new Request.Builder()
                                                 .url(url4)
                                                 .post(body)
                                                 .addHeader("Authorization", tkn)
                                                 .addHeader("Content-Type", "application/json")
                                                 .build();
                                         try {
                                             Response response4 = client.newCall(request4).execute();
                                             if (response4.isSuccessful()) {
                                                 String resp = response4.body().string();
                                                 JSONArray jsonarray4 = new JSONArray(resp);
                                             Log.i("dynavarr", String.valueOf(jsonarray4));
                                                 for (int k = 0; k < jsonarray4.length(); k++) {
                                                     JSONObject js = jsonarray4.getJSONObject(k);
                                                     options.put(js); }


                                             }
                                         }
                                         catch (IOException e) {
                                             e.printStackTrace();
                                         }


                                         Log.i("dans champ", label );
                                         Log.i("champ de la base", type);
                                     }
                                }
                                else {options.put(jo); }

                                Exampleitem it = new Exampleitem(id, type, label, placeholder, options);
                                itemList.add(it);

                            }
                        }


                        return itemList;

                    } catch (IOException e) {
                        e.printStackTrace();
                        return itemList;
                    }

                    //return List;

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    return itemList;
                }






                //return List;

            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return itemList;
            }

        }



        protected void onPostExecute(ArrayList<Exampleitem> result) {

            Log.i("liste formulaire", String.valueOf(itemList));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0,15,0,15);

            for (int i=0; i < itemList.size(); i++) {
                Exampleitem it = itemList.get(i);
                String type = it.getType();
                Log.i("iteeem", type);
                switch(type) {
                    case "text":

                    case "textarea":

                        EditText et = new EditText(thiscontext);
                        et.setHint(it.getLabel());
                        et.setTag(it.getId());
                        et.setTextSize(15);
                        et.setWidth(800);
                        et.setPadding(5,5,5,7);
                        et.setTextColor(Color.parseColor("#000000"));
                        et.setId(total);
                        et.setLayoutParams(params);
                        et.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary),
                                PorterDuff.Mode.SRC_ATOP);
                        total= total+1;
                        linearLayout.addView(et);
                        break;

                    case "title":
                        TextView tex = new TextView(thiscontext);
                        tex.setText(it.getLabel());
                        tex.setTextSize(20);
                        tex.setPadding(5,5,5,7);

                        tex.setTextColor(Color.parseColor("#1A237E"));
                        tex.setLayoutParams(params);
                        tex.setGravity(Gravity.CENTER_HORIZONTAL);
                        linearLayout.addView(tex);
                        break;

                    case "subtitle":
                        TextView te = new TextView(thiscontext);
                        te.setText(it.getLabel());
                        te.setTextSize(15);
                        te.setPadding(5,5,5,7);
                        te.setTextColor(Color.parseColor("#0467B5"));
                        te.setLayoutParams(params);
                        linearLayout.addView(te);
                        break;

                    case "dropdown":
                        ArrayList<String> spinnerArray = new ArrayList<String>();
                        JSONArray op = it.getOptions();
                        for (int j = 0; j < op.length(); j++) {
                            try {
                                JSONObject ob = op.getJSONObject(j);
                                String valeur = ob.getString("value");
                                if (ob.has("text")) {
                                    valeur = ob.getString("text");
                                }
                                spinnerArray.add(valeur);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        Spinner spinner = new Spinner(thiscontext);
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(thiscontext, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
                        spinner.setAdapter(spinnerArrayAdapter);
                        spinner.setTag(it.getId());
                        spinner.setId(total);
                        spinner.setSelected(false);
                        spinner.setSelection(0,true);
                        spinner.setLayoutParams(params);
                        total= total+1;
                        linearLayout.addView(spinner);

                        break;


                    case "submit":

                        Button btn = new Button(thiscontext);
                        btn.setText(it.getLabel());
                        btn.setWidth(227);
                        btn.setHeight(60);
                        btn.setId(0);
                        btn.setPadding(5,5,5,5);
                        btn.setBackgroundColor(Color.parseColor("#1A237E"));
                        btn.setHeight(77);
                        btn.setPadding(10, 25, 10, 10);
                        btn.setGravity(Gravity.CENTER_HORIZONTAL);
                        btn.setLayoutParams(params);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        linearLayout.addView(btn, lp);
                        break;


                    case "radio":
                        TextView rd = new TextView(thiscontext);
                        rd.setText(it.getLabel());
                        rd.setEms(12);
                        linearLayout.addView(rd);

                        JSONArray opt = it.getOptions();
                        int n =opt.length();
                        final RadioButton[] rb = new RadioButton[n];
                        RadioGroup rg = new RadioGroup(thiscontext); //create the RadioGroup
                        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
                        for(int h=0; h<n; h++){
                            rb[h]  = new RadioButton(thiscontext);
                            try {
                                rb[h].setText(opt.getJSONObject(h).getString("value"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            rb[h].setId(h + 200);
                            try {
                                rb[h].setTag(opt.getJSONObject(h).getString("value"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            rg.addView(rb[h]);
                        }
                        rg.setTag(it.getId());
                        rg.setId(total);
                        rg.setPadding(5,5,5,7);
                        rg.setLayoutParams(params);
                        total= total+1;
                        linearLayout.addView(rg);
                       break;

                    case "image":
                        ImageView im = new ImageView(thiscontext);

                        Picasso.get()
                                .load(it.getLabel())
                                .into(im);
                        linearLayout.addView(im);
                        break;

                    case "datetime" :
                        EditText dt = new EditText(thiscontext);
                        dt.setHint(it.getLabel());
                        dt.setTextSize(15);
                        dt.setWidth(800);
                        dt.setPadding(5,5,5,7);
                        dt.setTextColor(Color.parseColor("#000000"));
                        dt.setLayoutParams(params);
                        dt.setTag(it.getId());
                        dt.setId(total);
                        dt.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary),
                                PorterDuff.Mode.SRC_ATOP);
                        if (it.getLabel().toLowerCase().contains("date")) {
                            dt.setFocusable(false);
                            dt.setClickable(true);
                            final Calendar myCalendar = Calendar.getInstance();
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    updateLabel(dt, myCalendar);
                                }

                            };

                            dt.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    new DatePickerDialog(thiscontext, date, myCalendar
                                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                                }
                            });

                            dt.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);

                            total= total+1;
                            linearLayout.addView(dt);
                    } else {
                            dt.setFocusable(false);
                            dt.setClickable(true);
                            dt.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    Calendar mcurrentTime = Calendar.getInstance();
                                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                                    int minute = mcurrentTime.get(Calendar.MINUTE);

                                    TimePickerDialog mTimePicker;
                                    mTimePicker = new TimePickerDialog(thiscontext, new TimePickerDialog.OnTimeSetListener() {
                                        @Override
                                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                            dt.setText( selectedHour + ":" + selectedMinute);
                                        }
                                    }, hour, minute, true);
                                    mTimePicker.setTitle("Select Time");
                                    mTimePicker.show();

                                }
                            });
                            dt.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);

                            total= total+1;
                            linearLayout.addView(dt);

                    }
                        break;

                }

            }




        Button btn = (Button) linearLayout.findViewById(0);
            JSONObject js = new JSONObject();

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FormBody.Builder formBuilder = new FormBody.Builder();
                    Boolean test = true;
                    for (int i=1; i < total; i++) {
                        String id = (String) linearLayout.findViewById(i).getTag();
                        View vi = linearLayout.findViewById(i);
                        String value ="";

                        if (vi instanceof EditText ) {
                            value = ((EditText) vi).getText().toString();
                            if(((EditText) vi).length()==0 && !(vi.getTag().equals("commentaire") )) {
                                ((EditText)vi).setError("Veuillez remplir ce champ!");
                                test = false;
                            }
                        }
                           else if (vi instanceof Spinner) {
                             Spinner m = (Spinner) linearLayout.findViewById(i);
                            if (m.getSelectedItem() !=null) {
                             value = m.getSelectedItem().toString();
                             Log.i("value spinner", value);
                             }

                         }
                        else if (vi instanceof RadioGroup) {
                            int selectedId = ((RadioGroup) vi).getCheckedRadioButtonId();
                            RadioButton radioButton = (RadioButton) linearLayout.findViewById(selectedId);
                            if (selectedId != -1) {
                            value = (String) radioButton.getTag(); }
                            if (selectedId == -1) {
                                ((RadioButton)((RadioGroup) vi).getChildAt(0)).setError("Veillez remplir ce champs");
                                test = false;
                            }
                        }
                        formBuilder.add(id, value);
                        try {
                            js.put(id, value);

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }
                    if (test == true) {

                       // Log.i("bodyyy", String.valueOf(formBuilder));
                        //RequestBody body = formBuilder.build();
                        mAPIService = ApiUtils.getAPIService();
                        try {
                            sendPost(token, pro_id, tas_id, js, thiscontext);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //SendFormTask task = new SendFormTask(token, pro_id, tas_id, js, thiscontext);
                        //task.execute();
                    }

                }
            });

        }

        private void updateLabel(EditText dt, Calendar myCalendar) {
            String myFormat = "MM/dd/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            dt.setText(sdf.format(myCalendar.getTime()));
        }



    }

    private void sendPost(String token, String pro_id, String tas_id, JSONObject js, Context mContext) throws JSONException {
        JSONArray array = new JSONArray();
        array.put(js);
        Log.i("array", String.valueOf(array));

        PostForm body = new PostForm(pro_id, tas_id, js);

        String tkn = "Bearer " + token;
        mAPIService.postForm(tkn, body).enqueue(new Callback<Post>() {

            @Override
            public void onResponse(Call<Post> call, retrofit2.Response<Post> response) {

                if(response.isSuccessful()) {
                    Log.i("reponse API", "post submitted to API." + response.body().toString());
                    Toast.makeText(mContext,"Formulaire envoyé!", Toast.LENGTH_LONG).show();
                }
                if (response.code() == 400) {
                    try {
                        Log.v("Error code 400",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e("reponse API", "Unable to submit post to API.");
                Toast.makeText(mContext,"Erreur!", Toast.LENGTH_LONG).show();
            }
        });
    }



}
