package com.example.isi;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendFormTask extends AsyncTask<Void, Void, Boolean> {
    private final String mToken;
    private final String mPro_id;
    private final String mTas_id;
    private  Context mContext;
    private JSONObject mBody;

    SendFormTask(String token, String pro_id, String tas_id, JSONObject body, Context thiscontext) {
        mToken = token;
        mTas_id = tas_id;
        mPro_id=pro_id;
        mBody =body;
        mContext=thiscontext;
    }


    @Override
    protected Boolean doInBackground(Void... params) {

Log.i("in async", "in asyyyyync");
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");

        JSONArray array = new JSONArray();
        array.put(mBody);


        Log.i("array", String.valueOf(array));


        Uri builtUri = null;

            builtUri = Uri.parse("http://process.isiforge.tn/api/1.0/isi/cases")
                    .buildUpon()
                    .appendQueryParameter("pro_uid", mPro_id)
                    .appendQueryParameter("tas_uid", mTas_id)
                    .build();

        URL url = null;
        try {
             url = new URL(builtUri.toString());
             Log.i("urrrl", String.valueOf(url));
            Log.i("tokeeen", mToken);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String tkn = "Bearer " + mToken;
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // put your json here
        RequestBody body =  new FormBody.Builder().build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-type", "application/json")
                .addHeader("Authorization", tkn)
                .build();
        Log.i("requesttt", String.valueOf(request));
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resp = response.body().string();
                Log.i("réponse api", resp);
                return true ;
            } else {
                Log.i("réponse erreur", response.body().string());
                return false ;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }



    @Override
    protected void onPostExecute(final Boolean success) {
        if (success) {
            Toast.makeText(mContext,"Formulaire envoyé!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(mContext,"Erreur!", Toast.LENGTH_LONG).show();
        }
    }

}