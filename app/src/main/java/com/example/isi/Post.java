package com.example.isi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Post {

    @SerializedName("app_uid")
    @Expose
    private String appUid;
    @SerializedName("app_number")
    @Expose
    private Integer appNumber;

    public String getAppUid() {
        return appUid;
    }

    public void setAppUid(String appUid) {
        this.appUid = appUid;
    }

    public Integer getAppNumber() {
        return appNumber;
    }

    public void setAppNumber(Integer appNumber) {
        this.appNumber = appNumber;
    }

}
