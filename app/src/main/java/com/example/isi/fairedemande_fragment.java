package com.example.isi;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class fairedemande_fragment extends Fragment  {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Examplefairedemande> mList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        String token = bundle.getString("tkn");

        View rootView = inflater.inflate(R.layout.fragment_fairedemande, container, false);

        mRecyclerView = rootView.findViewById(R.id.recyclerView);

        GetProcessTask myTask = new GetProcessTask(token);

        myTask.execute();


        return rootView;


    }


    public class GetProcessTask extends AsyncTask<Void, Void, ArrayList<Examplefairedemande>> implements ExampleAdapter.ExampleViewHolder.OnNoteListener {
        private final String token;

        GetProcessTask(String tkn) {
            token = tkn;

        }

        @Override
        protected ArrayList<Examplefairedemande> doInBackground(Void... params) {
            ArrayList<Examplefairedemande> List = new ArrayList<>();
            OkHttpClient client = new OkHttpClient();


            String tkn = "Bearer " + token;
            Log.i("valeur token", tkn);
            Request request = new Request.Builder()
                    .url("http://process.isiforge.tn/api/1.0/isi/case/start-cases")
                    .addHeader("Authorization", tkn)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response responses = null;
            Log.i("la requete", String.valueOf(request));
            try {
                responses = client.newCall(request).execute();

                String jsonData = responses.body().string();

                JSONArray jsonarray = new JSONArray(jsonData);
              //  Log.i("liste process", String.valueOf(jsonarray));


                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                   // Log.i("objet process", String.valueOf(jsonobject));
                    String name = jsonobject.getString("pro_title");
                    String id = jsonobject.getString("pro_uid");
                    String tas = jsonobject.getString("tas_uid");

                    List.add(new Examplefairedemande(id, name, tas));
                }
                Log.i("résultat", List.toString());

                return List;

            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return List;
            }

        }

        protected void onPostExecute(ArrayList<Examplefairedemande> result) {

            mList = result;
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getContext());
            mAdapter = new ExampleAdapter(mList,  this);

            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);
        }

        @Override
        public void onNoteClick(int position) {

            Bundle arguments = new Bundle();
            arguments.putString("tkn", token);
            arguments.putString("id", mList.get(position).getId());
            arguments.putString("titre", mList.get(position).getText1());
            arguments.putString("tas", mList.get(position).getTas_uid());
            form_fragment form = new form_fragment();

            form.setArguments(arguments);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    form).commit();

        }
    }
}
