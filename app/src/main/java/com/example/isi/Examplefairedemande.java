package com.example.isi;

public class Examplefairedemande {

    private String mText1;
    private String id;
    private String tas_uid;

    public Examplefairedemande( String text1, String text2, String text3) {

        mText1 = text2;
        id = text1;
        tas_uid = text3;
    }


    public String getText1() {
        return mText1;
    }

    public String getId() {
        return id;
    }

    public String getTas_uid() {
        return tas_uid;
    }

    public String toString(){
        return "Id : " + id + "\nTexte : " + mText1;
    }
}
