package com.example.isi;

import org.json.JSONArray;

import java.util.ArrayList;

public class Exampleitem {

    private String type;
    private String id;
    private String label;
    private String placeholder;
    private JSONArray options;


    public Exampleitem(String text1, String text2, String text3, String text4, JSONArray text5) {

        id = text1;
        type = text2;
        label = text3;
        placeholder = text4;
        options = text5;
    }


    public JSONArray getOptions() {
        return options;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public String getLabel() {
        return label;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String toString(){
        return "Id : " + id + "\nType : " + type + "\nlabel" + label + "\nplaceholder" + placeholder + "\noptions" + options;
    }
}
