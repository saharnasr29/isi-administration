package com.example.isi;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ExampleAdapter extends RecyclerView.Adapter <ExampleAdapter.ExampleViewHolder> {
    private ArrayList<Examplefairedemande> mExampleList;
    private ExampleViewHolder.OnNoteListener mOnNoteListener;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView mTextView1;
        OnNoteListener OnNoteListener;

        public ExampleViewHolder(View itemView, OnNoteListener OnNoteListener) {
            super(itemView);

            this.OnNoteListener = OnNoteListener;
            mTextView1 = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           OnNoteListener.onNoteClick(getAdapterPosition());
        }

        public interface OnNoteListener{
            void onNoteClick(int position);
        }
    }

    public ExampleAdapter(ArrayList<Examplefairedemande> exampleList, ExampleViewHolder.OnNoteListener OnNoteListener) {
        mExampleList = exampleList;
        this.mOnNoteListener = OnNoteListener;
    }

    @Override
    public ExampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_fairedemande, parent, false);
        ExampleViewHolder evh = new ExampleViewHolder(v, mOnNoteListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(ExampleViewHolder holder, int position) {
        Examplefairedemande currentItem = mExampleList.get(position);


        holder.mTextView1.setText(currentItem.getText1());

    }


    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}


