package com.example.isi;

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "http://process.isiforge.tn/api/1.0/isi/cases/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}