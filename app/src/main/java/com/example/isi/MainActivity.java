package com.example.isi;

import androidx.appcompat.app.AppCompatActivity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.btnLogin);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                EditText username = (EditText)findViewById(R.id.etUsername);
                EditText password = (EditText)findViewById(R.id.etPassword);
                Boolean test = true;
                if (username.getText().toString().length() == 0)
                {
                    username.setError("Veuillez entrer un username!");
                    test = false;

                }
                if (password.getText().toString().length() == 0)
                {
                    password.setError("Veuillez entrer un mot de passe!");
                    test = false;

                }
                if (test == true) {
                    UserLoginTask myTask = new UserLoginTask(username.getText().toString(), password.getText().toString());


                    myTask.execute();
                }
            }
        });

    }




    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        private final String mEmail;
        private final String mPassword;
        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }


        @Override
        protected Boolean doInBackground(Void... params) {


            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body =  new FormBody.Builder()
                    .add("grant_type","password")
                    .add("username", mEmail)
                    .add("password", mPassword)
                    .add("scope", "*")
                    .add("client_id", "WMZNSSETCJDPTZSVETRNOPGYFKMAKHHQ")
                    .add("client_secret", "5813427175e8e5d18452a90035077331")
                    .build();


            Request request = new Request.Builder()
                    .url("http://process.isiforge.tn/isi/oauth2/token")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    String resp = response.body().string();
                    JSONObject object = (JSONObject) new JSONTokener(resp).nextValue();
                    String token = (String) object.get("access_token");
                    Intent intent = new Intent(MainActivity.this, MenuActivity.class ) ;
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("token", token);
                    getApplicationContext().startActivity(intent);
                    return true ;
                } else {

                    return false ;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }



        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                finish();
            } else {
                Toast.makeText(MainActivity.this,"Username ou mot de passe incorrect!", Toast.LENGTH_LONG).show();
            }
        }

    }






}
