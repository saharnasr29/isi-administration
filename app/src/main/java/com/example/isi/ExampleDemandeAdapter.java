package com.example.isi;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ExampleDemandeAdapter extends RecyclerView.Adapter <ExampleDemandeAdapter.ExampleViewHolder> {
    private ArrayList<Exampledemande> mExampleList;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        public TextView process;
        public TextView number;
        public TextView last_update;
        public TextView status;


        public ExampleViewHolder(View itemView) {
            super(itemView);

            process = itemView.findViewById(R.id.process);
            number = itemView.findViewById(R.id.number);
            last_update = itemView.findViewById(R.id.last_update);
            status = itemView.findViewById(R.id.status);

        }
    }

    public ExampleDemandeAdapter(ArrayList<Exampledemande> exampleList) {
        mExampleList = exampleList;
    }

    @Override
    public ExampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_demande, parent, false);
        ExampleViewHolder evh = new ExampleViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(ExampleViewHolder holder, int position) {
        Exampledemande currentItem = mExampleList.get(position);


        holder.process.setText(currentItem.getProcess());
        holder.number.setText(currentItem.getNumber());
        holder.status.setText(currentItem.getStatus());
        holder.last_update.setText(currentItem.getLast_update());

    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}


